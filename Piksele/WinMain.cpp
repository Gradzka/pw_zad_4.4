#include <Windows.h>
#include "commctrl.h"
#include <iostream>
#include "stdlib.h"

#pragma comment(lib, "comctl32.lib")
#pragma warning (disable:4996)

//COLOUR PRAGMA
#pragma comment(linker, "/manifestdependency:\"type='win32' \
    name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
    processorArchitecture='*' \
    publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "comctl32.lib")

#define m_rows 5 //matrix pomocniczy
#define m_columns 5 //matrix pomocniczy
#define rozmiar_piksela 50

LPSTR NazwaKlasy = "Klasa Okienka";
MSG Komunikat; //globalna zmienna do przechowywania komunikatow
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

HWND HWND_Button_Matrix[m_rows*m_columns];
volatile HWND hwnd;

DWORD dwThreadId[m_rows*m_columns];
HANDLE hThread[m_rows*m_columns];
//------------------------------------------------------------------------
//					  MATRIX - tablica pomocnicza
//------------------------------------------------------------------------
volatile int **create_matrix(int rows, int columns)
{
	volatile int **matrix = new volatile int*[rows];
	for (int i = 0; i < rows; i++)
		matrix[i] = new volatile int[columns];
	return matrix;
}
void destroy_matrix(volatile int **&matrix, int rows, int columns)
{
	for (int i = 0; i < rows; ++i)
		delete[] matrix[i];

	delete[] matrix;
}
//------------------------------------------------------------------------
//		0	1	0	1	0
//		1	0	1	0	1
//		0	1	0	1	0
//		1	0	1	0	1
//		0	1	0	1	0
//------------------------------------------------------------------------
void zero_one_matrix(volatile int **&matrix, int rows, int columns)
{
	int state = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matrix[i][j] = state;
			if (state == 0) state = 1;
			else state = 0;
		}
	}
}
void zero_matrix(volatile int **&matrix, int rows, int columns)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matrix[i][j] = 0;
		}
	}
}
volatile int **stan_watku = create_matrix(m_rows, m_columns); //tablica, ktora zawiera info o tym, czy dany watek dziala
CRITICAL_SECTION CriticalSection;
void change_matrix(volatile int **&matrix, int i, int j)
{
	/*                I                 */
	if (i == 0 && j > 0 && j < m_columns - 1)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[m_rows - 1][j] == 0 && stan_watku[i + 1][j] == 0 && stan_watku[i][j - 1] == 0 && stan_watku[i][j + 1] == 0)
		{
		stan_watku[i][j] = 1;
		LeaveCriticalSection(&CriticalSection);
		matrix[i][j] = matrix[m_rows - 1][j] + matrix[i + 1][j] + matrix[i][j - 1] + matrix[i][j + 1];
		stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}

	}
	/*                II                */
	else if (i > 0 && i < m_columns - 1 && j == m_rows - 1)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[i - 1][j] == 0 && stan_watku[i + 1][j] == 0 && stan_watku[i][j - 1] == 0 && stan_watku[i][0] == 0)
		{
			stan_watku[i][j] = 1;
			LeaveCriticalSection(&CriticalSection);
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[i - 1][j] + matrix[i + 1][j] + matrix[i][j - 1] + matrix[i][0];
			stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	}
	/*                III               */
	else if (i == m_rows - 1 && j>0 && j < m_columns - 1)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[i - 1][j] == 0 && stan_watku[0][j] == 0 && stan_watku[i][j - 1] == 0 && stan_watku[i][j + 1] == 0)
		{
			stan_watku[i][j] = 1; 
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[i - 1][j] + matrix[0][j] + matrix[i][j - 1] + matrix[i][j + 1];
			stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	}
	/*                IV               */
	else if (i > 0 && i < m_columns - 1 && j == 0)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[i - 1][j] == 0 && stan_watku[i + 1][j] == 0 && stan_watku[i][m_columns - 1] == 0 && stan_watku[i][j + 1] == 0)
		{
			stan_watku[i][j] = 1;
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[i - 1][j] + matrix[i + 1][j] + matrix[i][m_columns - 1] + matrix[i][j + 1];
			stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	}
	/*                V                */
	else if (i == 0 && j == 0)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[m_rows - 1][j] == 0 && stan_watku[i + 1][j] == 0 && stan_watku[i][m_columns - 1] == 0 && stan_watku[i][j + 1] == 0)
		{
			stan_watku[i][j] = 1;
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[m_rows - 1][j] + matrix[i + 1][j] + matrix[i][m_columns - 1] + matrix[i][j + 1];
			stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	
	}
	/*                VI               */
	else if (i == 0 && j == m_columns - 1)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[m_rows - 1][j] == 0 && stan_watku[i + 1][j] == 0 && stan_watku[i][j - 1] == 0 && stan_watku[i][0] == 0)
		{
			stan_watku[i][j] = 1;
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[m_rows - 1][j] + matrix[i + 1][j] + matrix[i][j - 1] + matrix[i][0];
			stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	}
	/*                VII              */
	else if (i == m_rows - 1 && j == m_columns - 1)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[i - 1][j] == 0 && stan_watku[0][j] == 0 && stan_watku[i - 1][j] == 0 && stan_watku[i][0] == 0)
		{
			stan_watku[i][j] = 1;
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[i - 1][j] + matrix[0][j] + matrix[i - 1][j] + matrix[i][0];
			stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	}
	/*                VIII             */
	else if (i == m_rows - 1 && j == 0)
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[i - 1][j] == 0 && stan_watku[0][j] == 0 && stan_watku[i][m_columns - 1] == 0 && stan_watku[i][j + 1] == 0)
		{
			stan_watku[i][j] = 1;
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[i - 1][j] + matrix[0][j] + matrix[i][m_columns - 1] + matrix[i][j + 1];
			stan_watku[i][j] = 0;
		}
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	}
	/*                IX               */
	else
	{
		EnterCriticalSection(&CriticalSection);
		if (stan_watku[i - 1][j] == 0 && stan_watku[i + 1][j] == 0 && stan_watku[i][j - 1] == 0 && stan_watku[i][j+1] == 0)
		{
			stan_watku[i][j] = 1;
			LeaveCriticalSection(&CriticalSection);
			matrix[i][j] = matrix[i - 1][j] + matrix[i + 1][j] + matrix[i][j - 1] + matrix[i][j+1];
			stan_watku[i][j] = 0;
		}
		/*if (stan_watku[i - 1][j] != 0 || stan_watku[i + 1][j] != 0 || stan_watku[i][j - 1] != 0 || stan_watku[i][j + 1] != 0)
		{
			if (stan_watku[i - 1][j] != 0)
			MessageBox(NULL, "UP" , "Window Class", MB_ICONEXCLAMATION | MB_OK);
			if ( stan_watku[i + 1][j] != 0)
				MessageBox(NULL, "DOWN", "Window Class", MB_ICONEXCLAMATION | MB_OK);
			if (stan_watku[i][j - 1] != 0 )
				MessageBox(NULL, "LEFT", "Window Class", MB_ICONEXCLAMATION | MB_OK);
			if (stan_watku[i][j+1] != 0)
				MessageBox(NULL, "RIGHT" , "Window Class", MB_ICONEXCLAMATION | MB_OK);
		}*/
		else {
			LeaveCriticalSection(&CriticalSection);
		}
	}
	if (matrix[i][j] > 8) matrix[i][j] %= 9;
}

volatile int **matrix = create_matrix(m_rows, m_columns); //globalna tablica matrix
//------------------------------------------------------------------------
//			USTAWIANIE WSPOLRZEDNYCH X i Y  - STATIC BUTTON
//------------------------------------------------------------------------

void ustaw_x(int j, int &x)
{
	if (j == 0) x = 0;
	else if (j == 1) x += rozmiar_piksela;
	else if (j == 2) x += rozmiar_piksela;
	else if (j == 3) x += rozmiar_piksela;
	else if (j == 4) x += rozmiar_piksela;
}
void ustaw_y(int i,int &y)
{
	if (i == 0) y = 0;
	else if (i == 1) y += rozmiar_piksela;
	else if (i == 2) y += rozmiar_piksela;
	else if (i == 3) y += rozmiar_piksela;
	else if (i == 4) y += rozmiar_piksela;
}
void Button(HINSTANCE hInstance)
{
	int x = 0, y = 0;
	int button_id = 0;
	for (int j = 0; j < m_rows; j++){
		ustaw_x(j, x);
		for (int i = 0; i < m_columns; i++)
		{
			ustaw_y(i, y);
			HWND_Button_Matrix[button_id] = CreateWindow("BUTTON", "", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON|BS_OWNERDRAW,
				y, x, rozmiar_piksela, rozmiar_piksela, hwnd, (HMENU)button_id, hInstance, NULL);
			//HWND_Button_Matrix[button_id] = CreateWindowEx(0, "STATIC", "", WS_CHILD | WS_VISIBLE | SS_CENTER | SS_CENTERIMAGE, y, x, rozmiar_piksela, rozmiar_piksela, hwnd, (HMENU)static_id, hInstance, NULL);
			button_id += 1;
		}
	}
	
}
//------------------------------------------------------------------------
//					  KOLOROWANIE STATIC CONTROLS
//------------------------------------------------------------------------
void paint_button_controls(WPARAM wParam,LPDRAWITEMSTRUCT lpDIS, int rows, int columns){

			if (matrix[wParam/rows][wParam%columns] == 0)		//RED
			{
				SetDCBrushColor(lpDIS->hDC, RGB(255, 0, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 1) //YELLOW
			{
				SetDCBrushColor(lpDIS->hDC, RGB(255, 255, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 2)	//BRIGHT GREEN
			{
				SetDCBrushColor(lpDIS->hDC, RGB(128, 255, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 3)	//BRIGHT BLUE
			{
				SetDCBrushColor(lpDIS->hDC, RGB(0, 255, 255));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 4)	//MEDIUM BLUE
			{
				SetDCBrushColor(lpDIS->hDC, RGB(0, 128, 128));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 5)	//DARK GREEN
			{
				SetDCBrushColor(lpDIS->hDC, RGB(64, 128, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 6)	//PINK
			{
				SetDCBrushColor(lpDIS->hDC, RGB(255, 0, 128));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 7)	//BROWN
			{
				SetDCBrushColor(lpDIS->hDC, RGB(64, 0, 0));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
			else if (matrix[wParam / rows][wParam%columns] == 8)	//PURPLE
			{
				SetDCBrushColor(lpDIS->hDC, RGB(64, 0, 128));
				SelectObject(lpDIS->hDC, GetStockObject(DC_BRUSH));
				RoundRect(lpDIS->hDC, lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.right, lpDIS->rcItem.bottom, 0, 0);
			}
}
//------------------------------------------------------------------------
//					KLASA OKNA APLIKACJI - Tworzenie okna 
//------------------------------------------------------------------------
void application_window(WNDCLASSEX &wc, HINSTANCE hInstance){
	wc.cbSize = sizeof(WNDCLASSEX);					//rozmiar struktury w bajtach
	wc.style = 0;									//style klasy
	wc.lpfnWndProc = WndProc;						//wsk. do procedury oblugujacej okno
	wc.cbClsExtra = 0;								//dodatkowe bajty pamieci dla klasy
	wc.cbWndExtra = 0;								//j.w.
	wc.hInstance = hInstance;						//identyfikator aplikacji, ktora ma byc wlascicielem okna
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);		//ikonka okna
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);		//kursor myszy
	wc.hbrBackground = (HBRUSH)1;					//tlo ok
	wc.lpszMenuName = NULL;
	wc.lpszClassName = NazwaKlasy;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
}
void creation_window_application(HINSTANCE hInstance){
	RECT desktop;
	int horizontal = 0, vertical = 0, width = 270, height = 293;
	const HWND hDesktop = GetDesktopWindow();	// Get a handle to the desktop window
	GetWindowRect(hDesktop, &desktop);			//Get the size of screen to the variable desktop
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;

		hwnd = CreateWindowEx(
		WS_EX_CLIENTEDGE,			//rozszerzone parametry okna, 0 oznacza ich brak
		NazwaKlasy,					//adres tablicy char z nazwa klasy
		"AUTOMAT KOM�RKOWY",		//adres lancucha z napisem tytulowym okna
		WS_OVERLAPPEDWINDOW,		//styl okna
		horizontal / 2 - width / 2, //wspolrzedna X lewego gornego naroznika okna
		vertical / 2 - height / 2,	//wspolrzedna Y --||--
		width,						//szerokosc okna w pikselach, moze byc w CW_USEDEFAULT
		height,						//wysokosc okna w pikselach, moze byc w CW_USEDEFAULT
		NULL,						//uchwyt okna rodzicielskiego, jesli istnieje
		NULL,						//uchyt menu dla naszego okna
		hInstance,					//uchwyt programu, ktory korzysta z okna
		NULL);						//adres dodatkowych danych
}
//------------------------------------------------------------------------
//								THREADS
//------------------------------------------------------------------------

DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
	while (1)
	{		
		change_matrix(matrix, (int)lpParameter / m_rows, (int)lpParameter % m_columns);
		InvalidateRect(hwnd, NULL, FALSE);
		Sleep(500);
		
	}
	return 0;
}
void thread_creator()
{
	for (int i = 0; i < m_rows*m_columns; i++)
	{
		hThread[i] = CreateThread(NULL, 0, ThreadProc, (PVOID)i, 0, &dwThreadId[i]);
		if (hThread[i] == NULL) ExitProcess(3);
	}
}
//------------------------------------------------------------------------
//							 WINAPI MAIN  
//------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance	//uchwyt aplikacji
	, HINSTANCE hPrevInstance,			//uchyt poprzedniego wystapienia aplikacji, zawsze NULL
	LPSTR lpCmdLine,					//zawiera linie polecen, z jakiej zostal uruchomiony program, LPSTR synonim *char
	int nCmdShow)						//okresla stan okna programu
{
	//------------------------------------------------------------------------
	//					  MATRIX - tablica pomocnicza
	//------------------------------------------------------------------------
	zero_one_matrix(matrix, m_rows, m_columns);
	//------------------------------------------------------------------------
	//						KLASA OKNA APLIKACJI - Rejestrowanie klasy okna
	//------------------------------------------------------------------------
	WNDCLASSEX wc;
	application_window(wc, hInstance);

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Error", "Window Class",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}
	creation_window_application(hInstance); // Tworzenie okna

	if (hwnd == NULL)
	{
		MessageBox(NULL, "Error", "Window Error", MB_ICONEXCLAMATION);
		return 1;
	}

	//------------------------------------------------------------------------
	//					PROGRESS BAR, BUTTON, STATIC BUTTON
	//------------------------------------------------------------------------
	Button(hInstance);
	InitializeCriticalSection(&CriticalSection);
	//------------------------------------------------------------------------
	//									THREADS
	//------------------------------------------------------------------------
	zero_matrix(stan_watku, m_rows, m_columns);
	thread_creator();

	ShowWindow(hwnd, nCmdShow);	//Pokazanie okna na ekranie
	UpdateWindow(hwnd);			//Uaktualniamy tresc okna
	//------------------------------------------------------------------------
	//					PETLA KOMUNIKATOW
	//------------------------------------------------------------------------
	while (GetMessage(&Komunikat, NULL, 0, 0))
	{
		TranslateMessage(&Komunikat);
		DispatchMessage(&Komunikat);
	}
	return Komunikat.wParam;
}
// OBS�UGA ZDARZE�
LRESULT CALLBACK WndProc(HWND hwnd,		//uchwyt okna
	UINT msg,		//kod wiadomosci
	WPARAM wParam, //parametr 16-bitowy
	LPARAM lParam)	//parametr 32-bitowy
{
	switch (msg)
	{
	case WM_CLOSE: //umozliwia zamkniecie okna
	{
		for (int i = 0; i < m_rows*m_columns; i++)
		{
			TerminateThread(hThread[i], 0);
			CloseHandle(hThread[i]);
		}

		destroy_matrix(matrix, m_rows, m_columns);
		DestroyWindow(hwnd);
	}
		break;
	case WM_DESTROY:
		DeleteCriticalSection(&CriticalSection);
		PostQuitMessage(0);
		break;

	case WM_COMMAND:
		break;
	case WM_DRAWITEM:
	{
		LPDRAWITEMSTRUCT lpDIS = (LPDRAWITEMSTRUCT)lParam;
		
		paint_button_controls(wParam, lpDIS, m_rows, m_columns);

		return TRUE;
		break;
	}

		
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	return 0;
}